# Changes in 0.3.6

* Implement write traits for exclusive references.

# Changes in 0.3.5

* Implement `ReadableMessage` for shared references.

# Changes in 0.3.4

* Implement `RenderableOnMinimal` for `Result<T, E>` where both implement it.

# Changes in 0.3.3

* Add `promote_to_mutable_writable_message` to MinimalWritableMessage.
* Increase Rust version to 1.79.

# Changes in 0.3.2

* Add `.with_static_type_annotation()` method to enable downcasting of non-'static types.

# Changes in 0.3.1

* Disable num-traits default features.

  This restores building on no-std systems.

# Changes in 0.3.0

* Remove `FromOtherOption` trait.
  The trait was not practically usable for lack of specializtion.

* Require `WithSortedOptions` for `set_from_message`.
  This was a practical requirement previously already,
  and is now expressed in the type system.

* Documentation updates.

# Changes in 0.3.0-alpha.2

* Add `convert_..._error` functions to `MinimalWritableMessage` to solve trouble in `coap-request` implementations.

# Changes in 0.3.0-alpha.1

* Write functions are fallible now,
  and can be retried with shorter content.
  The `set_code()` function remains infallible
  because all relevant data is checked at input construction time.

* There are now precise requirements on the calling sequence of methods in a `MinimalWritableMessage`.

* The new `RenderableOnMinimal` trait is provided as a convenience for errors that would eventually be rendered.

* The `Code` and `OptionNumber` traits now demands an explicit `try_from` method
  with a more restricted error type than is provided by the `TryFrom` trait.

* Remove all deprecated parts:
  * The `payload_mut` function was removed;
    use `payload_mut_with_len` instead.

  * The deprecated `HeapMessage` implementation was removed.
    There is no replacement;
    if need arises, it will be implemented in coap-message-utils.

* All features were removed:
  `typenum_test` was merely a dysfunctional experiment;
  `alloc` has become obsolete with the removal of the deprecated HeapMessage.

//! Traits for the errors of fallible operations
//!
//! This currently only contains traits for rendering onto a [`crate::MinimalWritableMessage`], as
//! errors are expected to be simple, and ideally widely reusable both on systems that only provide
//! `MinimalWritableMessage` and more powerful ones.
//!
//! Traits for rendering onto more powerful messages can be added on demand.

use core::fmt::Debug;

/// Indicates that an error type can be rendered into a CoAP response
///
/// The error may encode details as to what went wrong in the response. Implementors should be
/// aware that this may leak information, but it is ultimately the responsibility of the
/// application to decide which errors to pass on to the client, and which are to be filtered.
///
/// As rough guidance, it is generally expected that problems with the input would be represented
/// in the error (for example, an error would indicate which option was not understood, or where in
/// the payload parsing went wrong), but should not reveal details such as the local variables or a
/// stack trace to unauthorized clients.
///
/// This error trait is most useful in CoAP servers, both for errors that occur during processing
/// of the request and for errors that occur during preparation of the response.
///
/// In a CoAP client, it is less useful, as the error is likely to be handled internally and not
/// rendered (which is why [core::fmt::Debug] is a common co-occurring requirement). The trait can
/// still be useful to coerce both locally and remotely occurring errors into a consistent
/// perspective, as long as care is taken to not to send users on the wrong debugging path. Client
/// side errors should render in such a way that they can be sent to the origin client when
/// implementing a proxy.
///
/// While not on its own depending on Debug, it is usually combined with it.
pub trait RenderableOnMinimal {
    /// Error to return when even the error rendering is unsuccessful (a "double error").
    ///
    /// This is generic over an Inner Error, because rendering may happen on any type of message,
    /// each of which has its own write errors, so the total error often contains a message type
    /// dependent error.
    ///
    /// For Renderables that do not have "own" errors (i.e. where the only reason rendering would
    /// fail is if an operation on the MinimalWritableMessage fails), this can be set to `IE`. It
    /// is recommended that the there is a conversion `From<IE>` `Into<Self::Error>`, as that
    /// allows the [`Self::render()`] method to `?` out errors.
    type Error<IE: RenderableOnMinimal + Debug>: RenderableOnMinimal + Debug;

    /// Express the error in a CoAP response message
    ///
    /// The implementation should assume that the message is uninitialized, i.e., that it must set
    /// the code, options and payload in sequence.
    fn render<M: crate::MinimalWritableMessage>(
        self,
        message: &mut M,
    ) -> Result<(), Self::Error<M::UnionError>>;
}

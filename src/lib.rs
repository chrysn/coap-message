#![no_std]
//! CoAP mesage abstraction
//! =======================
//!
//! This crate defines interfaces for readable and writable [CoAP] messages, which are used in the
//! Internet of Things to transfer state in representations (REST). With these interfaces, it
//! allows CoAP servers and clients to be written independently of the actual CoAP library that is
//! used to serialize and deserialize the messages to the protocol.
//!
//! [CoAP]: http://coap.space/
//!
//! The defined traits only cover the properties of CoAP at the request/response exchange level,
//! not of the transport specific details. In particular, they do not expose tokens, message IDs or
//! message types: Those are not even present in some transports, and in general there is no need
//! for libraries to expose them. In the same vein, no methods are described to allocate messages:
//! For constrained devices, that may not even be possible, and with CoAP libraries that support
//! multiple configured transports, the type of message allocated may easily depend on the intended
//! use. They do not cover remote addresses either, as these too are transport specific.
//!
//! The crate also provides some trivial implementations of the types: [Code] for [u8], and
//! [OptionNumber] for [u16].
//!
//! Usage
//! -----
//!
//! A larger chunk of examples and demos is available at [coap-message-demos], which
//! illustrates both how to build coap-message based handlers, and how to use them in practice with
//! different implementations of CoAP. Most of the applications in there use coap-message through
//! [the implementations in coap-handler], which provide short-cuts for common cases.
//!
//! [coap-message-demos]: https://gitlab.com/chrysn/coap-message-demos
//! [the implementations in coap-handler]: https://gitlab.com/chrysn/coap-handler/-/blob/master/src/implementations.rs
//!
//! If you are on the implementing side of the message traits, you may want to have a look at
//! [coap-message-utils], which contains useful building blocks.
//!
//! [coap-message-utils]: https://crates.io/crates/coap-message-utils
//!
//! Error handling
//! --------------
//!
//! The readable message types are designed with minimal fallability -- for example, iterating over
//! the options can not raise a "premature end of message" style error, and the payload is always
//! "present" (albeit possibly empty). This encodes the concept that having a message type implies
//! validity of the message.
//!
//! Libraries that insist on minimizing the cycles spent parsing (as typical in embedded
//! situations) can opt to express the formatting error as a critical, not-safe-to-forward option
//! (eg. the private-use number 65535). As consumers of a message that has not been checked in
//! advance must check all options before acting on the message anyway, this ensures that the error
//! does not go unnoticed.
//!
//! Operations on writable messages are fallible. The typical error cause there is overflowing the
//! size of message that the transport can carry (the path MTU on UDP, or the peer's
//! Max-Message-Size on TCP) or the size of the allocated buffer. Errors resulting from the stack
//! not implementing certain features (eg. not implementing all options or codes) can be checked
//! ahead of attempted use because they use the associated types [Code] and [OptionNumber] that are
//! created fallibly.

mod message;
mod numbers;

pub use message::{
    MessageOption, MinimalWritableMessage, MutableWritableMessage, ReadableMessage,
    SeekWritableMessage, WithSortedOptions,
};

pub use numbers::{Code, OptionNumber};
pub mod error;
pub mod helpers;

mod core_impl;
